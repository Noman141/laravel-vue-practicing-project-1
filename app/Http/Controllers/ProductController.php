<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use File;

class ProductController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $products = Product::with('category')->get();
        return response()->json($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|unique:products',
            'category_id' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'nullable|numeric',
            'description' => 'nullable|string',
            'status' => 'numeric',

        ]);

        $product = Product::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'category_id' => $request->category_id,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
            'status' => 0,
        ]);
        //insert image
        if($product){
            if($request->image){
                $imageName = Str::slug($request->name).'-'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('/images/'), $imageName);
                $product->image = config('app.url',null).'/images/' . $imageName;
                $product->save();
            }
        }
        return response()->json('success',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id){
        $product = Product::with('category')->find($id);
        if ($product){
            return response()->json($product, 200);
        }else{
            return response()->json('failed', 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Product $product){
        $request->validate([
            'name' => "required|string|unique:products,name,$product->id",
            'slug' => "string",
            'category_id' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'nullable|numeric',
            'description' => 'nullable|string',
            'status' => 'numeric',

        ]);

        $product->update([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'category_id' => $request->category_id,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'description' => $request->description,
            'status' => 0,
        ]);

        if($product){
            if($request->image){
                $imageName = Str::slug($request->name).'-'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('/images/'), $imageName);
                $product->image = config('app.url',null).'/images/' . $imageName;
                $product->save();
            }
        }


        return response()->json('success',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product){
        if ($product){
            if ($product->image){
                $img = Str::after($product->image, 'http://127.0.0.1:8000/images/');
                if ($img){
                    unlink(public_path('/images/'.$img));
                }
                $product->delete();
                return response()->json('success','200');
            }else{
                return response()->json('failed',404);

            }

        }else{
            return response()->json('failed',404);
        }
    }
}
