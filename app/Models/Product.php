<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    use HasFactory;
    protected $guarded  = [];
//    protected $fillable = ['name','slug','category_id','price','quantity','description','status'];

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }
}
