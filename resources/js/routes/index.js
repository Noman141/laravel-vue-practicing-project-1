import Vue from 'vue';
import VueRouter from 'vue-router';
import HomeComponent from '../pages/home.vue'
import CategoryList from '../pages/category/index';
import CategoryCreate from '../pages/category/create';
import CategoryEdit from '../pages/category/edit';
import ProductList from '../pages/product/index';
import ProductCreate from '../pages/product/create';
import ProductEdit from '../pages/product/edit';
import AdminLogin from '../pages/auth/login';

Vue.use(VueRouter);


const routes = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: HomeComponent,
            name: 'home'
        },
        {
            path: '/category',
            component: CategoryList,
            name: 'category-list'
        },
        {
            path: '/category/create',
            component: CategoryCreate,
            name: 'category-create'
        },
        {
            path: '/category/edit/:id',
            component: CategoryEdit,
            name: 'category-edit'
        },
        {
            path: '/product',
            component: ProductList,
            name: 'product-list'
        },
        {
            path: '/product/create',
            component: ProductCreate,
            name: 'product-create'
        },
        {
            path: '/product/edit/:id',
            component: ProductEdit,
            name: 'product-edit'
        }
        ,
        {
            path: '/admin/login',
            component: AdminLogin,
            name: 'admin-login'
        }
    ]
})

export default routes;
